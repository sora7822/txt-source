◇【十河綾香】◇

自從被召喚到異世界之後已經過了好幾天。
十河綾香在位於王都西南方的森林裡走著。小心防範四周。
樹枝折斷的聲音。她立刻擺出應戰架勢。綾香手上拿的是長槍。

「咕嚕嚕嚕嚕⋯⋯吼啊啊啊啊啊啊啊啊！」

血盆大口不斷流出唾液的狗。
金眼。一口獠牙亮晃晃的血盆大口。嘴巴大得驚人。
在之前的世界，從沒看過這樣的生物。
異世界。原來這裡真的是異世界。她愈來愈能親身感受到這個事實。
她朝狗型的魔物踏出一步。

「要一較高下嗎？」

她也不清楚這句話究竟是在問自己，還是在問那只魔物？

「咕嚕嚕嚕嚕⋯⋯！」

張開血盆大口的狗以迎戰態勢威脅嚇唬她。

「跑哪去了啊啊啊啊啊啊啊！？可惡啊啊，那只臭狗喔喔喔喔喔喔喔喔！」

小山田翔吾的怒吼聲逐漸接近。樹葉摩擦的聲音向這裡逼近。
接著，手中扛著一把大劍的小山田從林蔭裡跳了出來。

「──！？吼，嗚嗚嗚！？」

看見小山田的瞬間，血盆大口的狗立刻向後跳開。它以敏捷的腿朝小山田的反方向衝了出去。
綾香感到疑惑。從自己的距離，能夠擋下那只狗的去路，但是──

「那傢伙是我的獵物，十河！？你少礙事喔！？你不是我們上級團體的一員，所以給我滾！我沒有理由把獵物讓給你！」

小山田拒絶綾香的幫忙。

「不過，如果你答應我的邀請，加入我們的團體──我可以考慮把那只狗讓給你喔！？你回去好好考慮一下將來的出路吧！？」

留下威脅的話語後，小山田再度消失在茂密的林蔭深處。
又變回獨自一人的綾香確認周圍。沒有奇怪的聲音。她稍微放鬆了警戒。

「⋯⋯呼！」

現在，2C的勇者都在女神命令下進行戰鬥訓練。
這裡是設置在王都旁邊的森林，專供訓練使用。
地區外頭圍著巨大的高牆。
據說裡面養了許多凶暴的魔物。
但是她也聽說，魔物的能力正符合現在勇者的程度。

（從邪惡魔者手中拯救世界，是嗎？）

綾香背靠著陡峭垂直的岩石，緊緊抱住長槍。

───

幾天前。女神叫學生們殺了在城裡捕捉到的魔物。

聽說目的是為了讓學生們習慣殺害生物這件事。
綾香當時正在城裡的一個房間睡覺。因為她被女神打暈了。
等她醒來的時候，學生們的「試練儀式」已經結束了。

但是聽說有些學生無法通過這個儀式。
他們無論如何都無法動手殺害魔物。

有人嘔吐、無法動彈。有人泣不成聲。有人昏厥過去。有人陷入錯亂。
這也是理所當然。因為必須親手奪走其他生物的性命。
大家長久以來都活在跟那種行為及思考無緣的世界。綾香自己也是。
綾香恢復意識的時候，女神來看她了。女神首先告訴她，班上出現了無法通過儀式的「落選者」。並告訴綾香，她打算「廢棄」他們的念頭。

『我也很遺憾，但是我不得不廢棄他們。他們跟已經過世的登河・三森將走上同一條末路⋯⋯』

女神順便簡短地告訴了她三森燈河最後的模樣。
只不過那跟自己認識的三森燈河似乎有點不同。

『⋯⋯⋯⋯』

不管怎麼樣，救不了他是事實。

『嘻嘻⋯⋯不好意思，十河同學⋯⋯你似乎很不甘心，但這是亞萊昂王國多年來不可動搖的方針。是這個世界的規律。我也無可奈何⋯⋯非常抱歉，真的非常抱歉⋯⋯』

綾香明白了女神的意圖。

『女神，我就說清楚吧！』
『什麼？請說！請儘管說清楚！』
『你那種做法，我無法贊同。』
『嗯～？什麼？你指的是哪種做法？』
『⋯⋯就是捨棄弱小的人，讓他們自生自滅的做法！』
『唔呵呵，這世界上有各種各樣的意見～我名為女神，心胸自然非常寬大。因此，我可以接納不一樣的意見喔～？只不過⋯⋯無法完成勇者職責的人，我必須請他們退場⋯⋯』
『Ｓ級勇者的價值很高，對吧？』
『沒錯！非～常高喔！』

綾香沒有選擇。她只能說出女神期望的答案。

『女神口中那些無法通過儀式的同學，我會連同他們的份，一起完成身為Ｓ級勇者的任務。所以，能不能取消「廢棄」他們的計畫？』
『哇啊！多麼勇氣十足的提議啊！真不愧是Ｓ級勇者！雖然我不習慣你的做法，但你一定願意粉身碎骨來協助我們吧！？為了拯救這個世界！』
『是的。』
『我真的好開心喔～！那麼，我們握手言和吧！雖說我是為了讓處於極度緊張狀態而精神錯亂的十河同學冷靜下來，但是我不應該揍你肚子的～』

綾香和女神握手。女神的手冷得像冰。
綾香心想：手冰的人都很溫柔，這句話根本是騙人的。

（錯亂，是嗎⋯⋯說得也是⋯⋯我們被召喚來這裡也不過是幾天前的事，但我卻無法好好想起任何資訊，看來我的腦袋真的很混亂⋯⋯）

但是她非得做不可──為了他們。
無法突破第一道試練的學生。
就她所聽到的名字，那些人全部都是班上最溫柔的人。
另外她還聽說，現在負責管理他們的是導師柘榴木。只不過現在的他也徹底失去氣力了。他醒來時看見的老師簡直和平時判若兩人。

『身為老師竟然是Ｄ級～！就算要我尊敬你，我也會無意識地抗拒啊～！好冷好冷啊～！啊，你想加入我們的團體，當然是不用了謝謝啊！對了！你去城裡的廚房洗盤子不就好了嗎！？柘榴木老弟！？』

小山田當面對他說的話，似乎讓他心裡受了重傷。
綾香提醒小山田，說他講得太過分了。綾香覺得柘榴木很可怜，所以對他說了幾句話。
但是，柘榴木卻對綾香視而不見，搖搖晃晃地離開了。

（⋯⋯我得變強才行。）

女神讓學生們自由選擇城堡裡的武器使用。
桐原拓斗選了刀，小山田翔吾選了巨大的劍，安智弘選擇雙劍，高雄聖挑了長劍，高雄樹則選了一把細劍。

這世界似乎存在著刀劍。
據說是過去召喚勇者帶來的武器與技術，普及到了部分地區。
十河綾香選擇了長槍。她毫不猶豫就選擇了這項武器。因為對綾香而言，這是她最熟悉的武器。

綾香的祖母是名為「鬼槍流」的古流武術師父。
綾香並不知道她是怎麼跟身為資產家的祖父結婚的。
從小，祖母就不斷教導綾香學習鬼槍流。
鬼槍流擅長的不只槍術。回溯根本，據說當敵人闖入長槍無法對應的距離時，所使用的武術也很發達。祖母是這樣教導她的。
讀書、學習、訓練之外，接受祖母教導武術也是綾香小小的樂趣。練完武術之後，淋浴沖掉汗水，讀完書再去睡覺。

這就是十河綾香的日常生活。現在回想起這些日子，甚至令人覺得懷念。

（我從沒想過練習古武術到底會對什麼有幫助，不過⋯⋯）

綾香非常喜歡祖母。所以才會繼續練習。

（沒想到，從奶奶那裡學到的鬼槍流，竟然會以這樣的形式派上用場⋯⋯）

但目前還無法安心。綾香若不能拿出一些成果，落選的學生就很危險了。
如果Ｓ級勇者無法得出女神期待的成果──

（大家就會像三森同學一樣⋯⋯）

捨棄弱者的想法，絶對是錯誤的。
貴族義務。
以前曾經流行一時的一句話。

擁有能力的人，必須盡其義務。非得好好表現不可。
綾香下定決心。首先得殺掉魔物，「提升等級」才行。
在這世界，似乎殺掉強大的魔物，自己也會變強。
據說殺了魔物，就能獲得經驗值（類似能量的東西嗎？）

「STATUS OPEN。」


～～～～～～～～～～～～～～～～～～～～

【綾香・十河】

LV1
HP：＋７００　MP：＋３００
攻擊：＋１３００　防御：＋３００　體力：＋５００
速度：＋７００【＋５００】　智慧：＋７００

【稱號：Ｓ級勇者】


～～～～～～～～～～～～～～～～～～～～

女神對她說明過。
只要等級提升，這個活動狀態（補正值）的數值就會增加。

等級這個概念，似乎只存在於召喚勇者之間。當地的人並沒有經驗值或補正值之類的概念。他們也不會確認自己的活動狀態。

【速度】下方的括弧是特殊道具的效果。
綾香輕輕觸摸耳環。據說大部分的特殊道具，都有提升補正值效果。她也覺得現在雙腳的速度的確比之前快了一點。

（經驗值⋯⋯）

殺了人類不能獲得經驗值。當然等級也不會提升。這的確是理所當然。如果殺了人就能輕易變強的話，那麼這個世界，殺人行為將會四處橫行。最糟的情況，勇者殺了應該保護的人類，也不無可能。暗殺一定也會頻繁發生吧！

另外，可以獲得經驗值的魔物據說有兩種。
擁有大量經驗值的，似乎是「金眼的魔物」

相對的，金眼魔物也更加厲害凶狠。

（還有⋯⋯）

技能項目顯示。她已經很習慣這項操作了。
綾香的固有技能仍是灰色字體。只顯示出【固有技能】而已。
總有一天，我會能使用類似魔法的力量嗎？
只不過，綾香至今還沒殺過一隻魔物。所以她的等級還維持在１。

（殺魔物⋯⋯我做得到嗎？不──）

非殺不可。為了救不了的三森燈河，也必須殺！
這次一定要成功保住大家。不能再讓女神殺掉任何一個同學了。要救助那些沒有力量的人！
不能讓三森燈河的死白費。

（好⋯⋯快去尋找魔物吧。）

綾香站起身來，邁出腳步。
她現在身上穿的服裝，並不是學校的制服。而是類似電影或小說中隨處可見的幻想世界居民，身上的打扮。
只不過她覺得，這華麗的設計，讓人過分意識到身為「女性」的事實。

『人家說面對異性時，讓對方能夠強烈感受到「異性」存在的裝扮，才能讓魔素的流動變得更豐沛喔～所以，我很在乎這些服裝外觀的設計。尤其是Ｓ級勇者的裝備，更不能偷工減料喔！』

女神是這麼說的。真的嗎？
綾香昨天自行補了一些布料上去，以減輕暴露的程度。

（穿這樣有點丟臉耶⋯⋯）

綾香非常討厭暴露身體。所以穿制服的時候，她也會穿上黑色褲襪。
話雖如此，但是只穿學校制服將會太缺乏防護能力也是事實。這套裝備上，似乎有女神的魔法（？）保護。現在，生命安危是最緊要的。多少也只能忍耐了。
她自暴自棄地邁開腳步。

（⋯⋯勇者，是嗎？）

這是現在令她覺得很棘手的一個詞彙。
簡直就像被人強迫要提起勇氣一樣。
請鼓起勇氣，面對邪惡！

勇者。魔法般的稱呼，讓人無路可逃。
這個稱呼，對現在的綾香而言，就像詛咒的名字。

「────！？」

綾香感受到有東西接近的氣息，她舉起長槍。

「呼啊、呼啊⋯⋯啊！十、十河同學！」
「⋯⋯鹿島同學？」

她應該是戰場淺蔥團體的成員才對。
現在，勇者們自然而然分成了好幾個小團體。
派系鬥爭。跟先前在教室的時候，一點也沒變。

「怎麼了？」
「有、有人要我向你傳話⋯⋯」
「你先喘口氣。我不會跑掉的。」
「對不起⋯⋯謝、謝謝你⋯⋯」

鹿島小鳩，在班上算是比較乖巧文靜的人。
但是，她會在這裡，就表示──

（她通過了女神的第一道試練⋯⋯）

因為她當時正在沉睡，所以並未看到現場的模樣。
只不過，她原以為小鳩是那種連虫子都不敢殺的人。或者是⋯⋯

（戰場同學搞不好動了什麼手腳⋯⋯）

戰場淺蔥給人的印象，是會動歪腦筋的人物。
她可能動了什麼手腳，讓小鳩「合格」了。
小鳩調整呼吸。

「那個⋯⋯城裡的人不小心搞錯，放了一隻非常厲害的小牛鬼魔物進來裡面了⋯⋯那只魔物看起來像是一個小小的牛人，可是⋯⋯所以⋯⋯現在立刻回去集合地點比較好⋯⋯」
「你特地跑過來，就是為了告訴我這件事嗎？」

在綾香的印象中，小鳩並不擅長運動。個性也略顯怯懦。

「唔，嗯⋯⋯因為十河同學是應該活下去的人⋯⋯」

她的說法，有點令人在意。簡直就像在說，真有萬一，自己死了也沒關係──

「鹿島同學？」

綾香發現了不對勁。小鳩的表情似乎凍結了。小鳩指著綾香背後。

「那、那個⋯⋯」

綾香回頭。

「噗哞嚕嚕嚕嚕嚕嚕嚕嚕！噗，吼喔喔喔喔喔────！」

牛頭人。身體算是嬌小的種類。但是──它充滿讓人恐懼的壓迫感。
金眼魔物。

「噗哼吼喔喔喔喔──！」

它向前衝了過來。

「鹿島同學，你快躲到我後方！盡量跑遠一點！我來對付它！」
「可、可是──」
「放心！包在我身上！」
「好、好！」

綾香擺出對戰架式。

（我辦得到嗎？）

她吸了一口氣，調整呼吸，止住心裡的顫抖。
她以眼睛捕捉對方的行動。綾香回想祖母說過的話。

『這招，最重要的就是時機和平常心！』

小牛鬼逼近。接觸──

啪！

「────────！」

咚，鏗鏗鏗鏗鏗！
小牛鬼面部朝上，狠狠地陷入地面之中──

「咳，嘔⋯⋯咳咳⋯⋯！？」

魔物口中吐出血沫。小牛鬼因為承受的衝擊，無法動彈。

「鬼槍流──」

制伏小牛鬼的是十河綾香。

「崩落十字⋯⋯」

練習武術之際，祖母要她使用招數時，發聲喊出招式名稱。祖母說這樣才會加深意識。她仍維持當時的習慣。

（成功了！）

利用對方力量和氣勢反擊的招式。大概比較接近合氣道的邏輯吧？
一開始，將槍插入對手的腋下一帶，擊垮支撐對手身體的「軸心」

緊接著，迅速扭轉自己的身體衝入對方懷中。利用對方的氣勢，以槍為準軸，架住對方的身體，這麼一來，最終階段前的十字型就完成了。
轉動身體，形成十字旋轉，將對手狠狠打入地面。
這就是鬼槍流──「崩落十字」

綾香以陰森森的表情，俯瞰著魔物。
以接近跨坐在對方身上的姿勢。
綾香反握長槍，以刃鋒對準魔物。

（殺了它⋯⋯我得殺了它⋯⋯）

為了變得更強。
綾香手臂用力。剎那間──

「讓開！十河────！」
「咦？」

身體被撞飛了。

「呀啊！？」

綾香跌坐在地上。
桐原拓斗取代她，站在她剛才俯瞰小牛鬼的位置。
桐原朝無法動彈的魔物伸出手臂。

「【金色龍鳴波】啊啊啊啊啊啊────！」

一道充滿攻擊性的金色光芒，消滅了小牛鬼。桐原呼吸急促地說：

「這麼一來──等級就有１８了⋯⋯！」

綾香愕然望著眼前的光景。小鳩用顫抖的聲音說：

「十河同學⋯⋯剛、剛才的⋯⋯」

桐原調整呼吸。過了一會兒，他呼出細細一口氣。
他望向綾香，用他平常那張冷酷的表情說：

「讓你協助我，辛苦你了。」
「咦？」

（協助⋯⋯？）

桐原看似無奈地吐了一口氣。

「剛才你差點就有危險了。可千萬別鬆懈啊，十河！」

桐原若無其事地轉過身去。
他就這樣直接離開了。

「剛才那是什麼情況⋯⋯」

小鳩目瞪口呆。似乎還無法處理混亂的腦袋。

「垃圾！」

突然傳來一道聲音，讓兩人嚇了一跳。小鳩也驚訝地發出「咦」一聲。
高雄聖不知道什麼時候站在附近了。

「他真是垃圾呢！」

綾香終於冷靜下來，明白剛剛發生了什麼事。
簡單來說，他剛剛橫刀搶走了綾香的經驗值。

（插圖０１１）

「⋯⋯聖同學，你真敢說話呢。」
「十河同學，你不在乎嗎？」
「我現在盡量不想在班上引起任何風波⋯⋯我想，桐原同學也還沒整理好心情，所以才會做出那種事──」
「你太天真了。你的想法要是那麼天真，很快就會死於非命喔？」
「⋯⋯或許吧。」
「我不太喜歡你那麼天真的想法呢。」
「⋯⋯嗯。」
「不過──」

聖轉過身去。

「也不是對你一點好感都沒有就是了。」

聖留下一句讓人不知如何解讀的話，揚長而去。
綾香稍後跟小鳩一起前往集合地點。

途中，她們注意到遠方天空有烏雲聚集起來。

（今後班上同學可能會為了爭奪金眼而發生爭執⋯⋯希望我不要成為爭端就好了⋯⋯）

十河綾香心中，也開始出現一片烏雲密布的天空。


□

那天，亞萊昂接獲一道消息。

以大陸最北端為據點的大魔帝軍隊，開始正式朝南方前進了。
因為大魔帝軍隊發動南征，使人稱北方要塞的瑪格納王國引以為傲的巨大城池──通稱「大誓壁」淪陷。
大誓壁淪陷後，大魔帝的軍隊便停止往南前進。

從那之後，就再也沒有侵略攻擊的氣息。
但是，局勢動蕩的消息，瞬間傳遍整座大陸，各國不得不立刻採取對策。
十河綾香在結束森林訓練後的第三天，才得知了這件事。


◇【三森燈河】◇

逃脫生存機率為零的廢棄遺跡。
邊晒著舒服的陽光，邊稍事休息──是不可能的。

『入口有一個只有定期前往遺跡的調查隊明白的標記⋯⋯那個標記從來不曾發生變化。』

這是遭到廢棄前，混帳女神所說的話。
我防範周圍，躲進柱子的陰影內。現在並沒有看到監視用的設施和監視者的身影。
我在腦內反芻女神說過的話。

「定期前往遺跡的調查隊」

也就是說，他們並不是隨時都在附近囉？
確認沒有人類的氣息後，我決定去遺跡入口一帶進行調查。
但是，那裡並沒有任何異狀。也沒看到那個標記。

「她好像說過那是『只有調查隊明白的標記』⋯⋯」

也就是說，想要在標記上動手腳是有困難的。
被他們發現我逃脫遺跡，也只是時間上的問題。

遺跡的門已經關起來了。
走出外頭的瞬間，遺跡大門以驚人的氣勢關了起來。有種被人家說「你快點滾出去！」的感覺。是因為我殺了太多魔物的關係嗎？
連逃脫時必須的金色寶石都來不及拿回來。我本來想，如果拿得回來，就把它拿去賣了換錢⋯⋯只可惜，人生在世沒那麼順利。
入口附近是一大片看似廢墟的風景。換言之，也可以說是深具文化遺産氣息、饒富古趣的風景。像是在教科書上可以看到的古代遺跡。

我迅速將附近調查過一次。遺跡地帶似乎位於森林之中。
我決定離開這裡。
逃脫遺跡時，已經道別過了。這裡沒我的事了。

稍微走了一會兒，我發現了一條感覺有人在使用的道路。泥土表層脫落的道路。
地面被踩踏得很堅固。是調查隊使用的道路嗎？
如果是的話，還是別走這條路比較好。

──隊長！路上有陌生的足跡！

萬一變成這樣的情況就麻煩了，因此我決定走在跟道路有點距離的森林之中。

「如果有個小池塘之類的地方，我好想洗洗身體啊⋯⋯」

雖然逃出遺跡了，但我該做的事還堆積如山。
我打算將該做的事逐一解決，還有我在遺跡中養成習慣的自言自語問題也得改掉才行。接
下來──

「STATUS OPEN。」


～～～～～～～～～～～～～～～～～～～～

【登河・三森】

LV1789
HP：＋５３６７　MP：＋５９０３７
攻擊：＋５３６７　防御：＋５３６７　體力：＋５３６７
速度：＋５３６７　智慧：＋５３６７

【稱號：Ｅ級勇者】


～～～～～～～～～～～～～～～～～～～～

只有MP足足多了十倍之多。但是，我現在想確認的不是這個。
我叫出技能項目。我想確認的是等級提升的兩種技能。


～～～～～～～～～～～～～～～～～～～～

【PARALYZE：LV3／消耗MP10／複數對象指定／任意解除／部位解除（頭部）】
【POISON：LV3／消耗MP10／複數對象指定／任意解除／非致死設定】


～～～～～～～～～～～～～～～～～～～～

項目增加了。兩種都追加了【任意解除】的項目。
在遺跡內並沒有讓我覺得有必要解除的局面。
因此，我並未想過這個問題，但是技能當時似乎無法任意解除。
不過，現在可以依照我的意思解除技能了。
如果附近有魔物的話，我等一下想嘗試看看。

另外還有【PARALYZE】的【部位解除（頭部）】

我看向括弧內的「頭部」兩個字。
我可以依照自己的意思，只解除頭部的技能嗎？讓對方動彈不得卻又想讓其說話時好像很有用。

接下來是【POISON】的【非致死設定】

簡單來說，就是可以設定讓對方處於中毒狀態，但並不會致死。
如果用電玩的方式來理解，就是把HP留下１點的感覺嗎？
這說不定很方便。但不是什麼值得讚賞的用法就是了。

「⋯⋯⋯⋯」

如果可以賦予效果的話，這招將會是最適合用在混帳女神身上的技能。

「有機會的話，我也想試看看【POISON】的追加能力。」

我關掉活動狀態顯示，從皮囊裡取出可樂。
這瓶可樂是相當久之前的東西了。最後一口。我決定從遺跡生還時要用它來慶祝。我一口氣乾了它。碳酸氣泡當然早就沒了。
不過這濃厚的甜味，深深撫平了身心的疲勞⋯⋯

「呼！」

我把空寶特瓶留下來。如果發現乾淨的河川，可以用來裝點水。
我再次出發。
應該思考的事有很多，但是先一件一件解決吧！
全部放在一起想的話，思考會混亂。我不擅長多工處理。

「接下來⋯⋯」

首先應該先尋找村莊或城鎮。

可以的話，我還想找間旅館好好休息，也想整理一下衣物。我很想盡快換掉長袍底下的制服，原本世界的制服太顯眼了。現在過度顯眼並無意義。
我也想知道自己身處的位置。這裡還是亞萊昂王國境內嗎？靠近中央嗎？或者位於邊境呢？又或者是在其他國家呢？若能弄到手的話，我還想要地圖。
現在的我，幸好還有在遺跡裡獲得的銀幣和寶石。不是身無分文，讓人放心多了。只不過，我必須先知道銀幣和寶石的價值。得稍微把握一下物價之類的東西才行。

大致上的資訊，召喚之後女神已經先說明給我們聽了。
但是，她並未說明細節部分。我認為有必要適當地確認一下。

「再來是──」

我朝肩上的皮囊一瞥。禁咒咒語書。還得搜集有關它的資訊才行。
因為它說不定是可以用來對付女神的力量。

「這麼說來⋯⋯」

我有可能自己學會嗎？只要請人教我上面所寫的語言，我也能使用禁咒？
還是說，能學會禁咒的人，有種族之類的限制？
關於這點，我也想趕快弄清楚。還有──

「真想要一把劍啊！」

我喃喃自語。
我的活動狀態，除了MP之外都很低。現在的我是這樣的想法。
我喚起在遺跡內的記憶。

後半幾乎沒有發生過體力耗竭的情況。連身體不舒服的感覺也沒有。腿部的疲勞也舒緩了。裝行李的皮囊，裡面的東西雖然增加了，感覺卻比以前更加輕盈。因此，我認為補正值確實出現了效果。

但是，噬魂魔和遺跡裡的魔物一直認定我是個「弱者」

即使突破LVl000了，我在遺跡中仍是「最弱」的。
說起來，我恐怕屬於負責後衛的魔術師類型。
並不是衝鋒陷陣的戰士類型。我心裡想著這些事情，所以才會脫口而出──

「真想要一把劍啊！」

──這樣的話。

「如果有個像盾牌角色一樣的護衛，就安心多了⋯⋯」

釋放狀態異常技能之際，如果有一把可以保護背後的「劍」，我就可以放心地專注於發動技能。
我在遺跡內活動時，盡量背靠著牆壁消除死角。只有在牆壁很多的遺跡內，才能用這個方式消除死角。但是在地面上，未必可行。
將來，或許可以僱用幾個武藝精湛的傭兵來對付女神。

「或者自己組一支傭兵團之類的。」

為了達成復仇的目的，我需要擬定更詳細、更完美的對策。
禁咒的力量還不清楚。託付太多希望在禁咒之上，太危險了。

「要是我的狀態異常技能對那個混帳女神有效，就可以更容易地完成復仇了，可是⋯⋯世事沒那麼簡單呢！」

沒辦法。不可能所有事都順心如意。反倒是我的情況，如果光看狀態異常技能和皮囊的能力，甚至會覺得一切都太順利了。

「再來，要找時間看一下從大賢者那裡獲得的『禁術大全』裡面寫了什麼──」

有一股氣息！我閉上嘴巴，提高警戒。感覺不到──殺氣。
但是，可以感受到攻擊性。是魔物嗎？
我躲在樹蔭後面觀察情況。

「嗶嘰！」「嗶嘰！？」「嗶嘰嘰嘰！」「嗶嘰！」

藍色圓形半透明、彈性十足的生物。

「那是──史萊姆嗎？」

大致數一下，有六只。
史萊姆。在RPG中，一開場幾乎都會出現的代表性魔物。
大致上都被設定為能力較弱的魔物。
感覺上一點也不厲害。也不像隱藏了什麼能力的樣子。
這令我重新認識到，遺跡裡的魔物真的強到非常驚人。

「嗯？那些史萊姆身上好像沒有金色的要素耶⋯⋯」

之前碰上的魔物，相當於眼球的部分多半是金色的。上半身像食虫植物的馬，也有類似金色眼球的部位。例外的只有連眼球都看不到的火龍僵屍而已吧。總之，現在這個時間點上，我並沒有在那些史萊姆身上看見金色的要素。

「⋯⋯⋯⋯」

對方並沒有注意到我。可能是現在沒空吧！因為⋯⋯

「嗶啾！」
「嗶嘰⋯⋯嘰⋯⋯！」
「嘰！」
「啾咿！」
「嘰！」
「嗶！？嗶嗶嗶～⋯⋯」
「嘰！」
「啾！？啾嗚嗚～⋯⋯」

它們發生了爭執。不──不能稱之為爭執。比較像單方面的霸凌。
一隻體型比其他更嬌小的史萊姆混在其中。它正遭到其他五只追趕攻擊。小史萊姆以讓人聯想到恐懼的動作後退。

──住、住手啦～！

我彷佛聽見它這麼說的聲音。
從氣氛來看，這並不像一般的打鬧。我決定暫時旁觀。
小史萊姆被壓得扁扁的。似乎一直在低聲下氣道歉。
我忍不住低聲說道：

「⋯⋯那樣不行啦。」

我並不是說不可以向他人求助。但就算求助，幫手也未必會來。
奮戰吧！最值得依靠的，應該是自己的力量才對啊！

「嗶嘰嘰嘰嘰～⋯⋯」

小史萊姆的顏色愈來愈淡。應該說，愈來愈接近灰色⋯⋯？

其他史萊姆打算殺了同種族的它嗎？
跟遺跡的魔物比起來，它們的殺意非常難讀取。
可能因為它們是弱小的魔物吧？很難判斷那是不是它們本身的殺意。

⋯⋯不行了。

就在我如此心想的時候。

「嗶嘰嘰嘰嘰嘰嘰嘰機嘰嘰嘰嘰────！」

被壓扁、顏色變淡的史萊姆跳了起來。

鏗！喀！

史萊姆互相撞擊，發出硬質的聲響。
它們攻擊時，似乎可以讓身體的一部分硬化。

「嗶嘰嘰嘰嘰嘰嘰！嗶嘎啊！」
「嗶嘰！嘰！」
「嗶嘰嘰嘰嘰嘰──啾嗚！？」

但是，多個打一個。一對五。嘗試反擊的史萊姆，身體比其他史萊姆更嬌小。

「嘰嘰嘰！嘰！」
「啾！？啾嗚！？」

它根本不可能贏。簡單來說──

「那樣就好了。」

我的嘴角，自然而然往上揚起。
我撥開草叢，往史萊姆的方向走。我伸出手臂。

「【PARALYZE】。」

史萊姆們的動作停止。

「嘰、啾！？」

混亂的氣氛支配了那群史萊姆。

「【POISON】。」

我對那五只史萊姆使用【POISON】

那群史萊姆染上中毒的顏色。視線角落浮現半透明的【致死】顯示。

「所以，可以從這裡改變設定嗎？」

我按了一下顯示。喀嚓一聲。

【致死】顯示切換成【非致死】
「立刻碰上可以試用的魔物，我還真是幸運呢！」

史萊姆傳來恐懼的感覺，是對死亡的恐懼嗎？
還是對我的恐懼呢？我無法確認他們是否對我有殺意。

「哞，吼吼，吼，吼吼⋯⋯！？」「噗，嘰，嘰⋯⋯」

我低頭看著五只史萊姆。

「呵呵⋯⋯我太幼稚了，不該擅自闖入的，真抱歉。只不過──你們這麼多只聯手毆打一隻明顯比較弱小的同伴，這幅情景我看了心裡不是很舒服呢⋯⋯所以，為了滿足我的感情，我才稍微出手捉弄你們一下。」

複數對象指定。我按下五只裡其中一隻黃色血條旁邊顯示的【解除】

顯示切換成【YES／NO】。我按下【YES】

這操作讓五只史萊姆的麻痺都解除了。中毒也同時解除了。
我收起笑容。

「滾！」

顏色變淡的史萊姆們邊防范著我邊向後爬。

「啾，喔喔⋯⋯喔喔⋯⋯」「嗶嘰嘰⋯⋯」

就像人拖著腳跟往後退的感覺。
靠我的活動狀態，能輕易擊敗這群史萊姆。我有這種信心。
加上，它們因為中毒的效果，現在可能瀕臨死亡。只要輕輕一踩，就能消滅它們吧！
五只史萊姆消失了踪影。只剩一隻小史萊姆，我轉向它。

「看它們那樣子，經驗值應該也不是很高⋯⋯而且萬一是這傢伙的家人，沒先問它的意見就殺了也不太好⋯⋯」

家人對親生的孩子產生殺意，是相當普通的事情。
對以前的我而言，很「普通」就是了。
我對小史萊姆說：

「我現在就幫你解除讓你不能動的『那個』⋯⋯你之後要幹嘛隨便你。啊啊⋯⋯我並沒有想要殺你的意思，所以你放心吧。」

可能是因為先前我感受到噬魂魔的想法之故吧？我會假設魔物聽得懂，而對它們說話。實際上，它們聽不聽得懂，我就不知道了。
我靠近小史萊姆，蹲下來。

「我接下來會說一些自以為很了不起的話喔？」
「嗶嘰？」

它一點也不怕我。奇怪的傢伙。

「抱歉這麼晚才過來幫你。只不過⋯⋯你幹得很好。在那種狀況下選擇反擊，真是大快人心。」
「嗶咿！」
「⋯⋯等你能動之後，可別攻擊我喔？」
「嗶！」

我們的對話好像可以成立⋯⋯不過也可能只是錯覺吧。
我解除【PARALYZE】

「嗶！嗶！嗶！」

嗯？顏色恢復了⋯⋯

史萊姆或許也一樣只要等時間經過，生命力就會恢復。我站起身來。

「再見，你要堅強地活著喔。」

我又自以為了不起地說了一句，重新背好皮囊，背對史萊姆跨出腳步。
總之，剛才成功試用了技能的新功能。雖然也有一些整理心情的意思在內，不過試用技能才是正題。還不賴，是一些可以使用的新功能。
我已經離開史萊姆那件事的現場一段距離了吧？

「⋯⋯」

後面一直有草叢晃動的聲音跟著我。
我回頭一看，嘆了口氣。我就知道。
剛才那只小史萊姆全身上下沾滿樹葉，跟上來了。
我用指尖搔搔額頭。

「你不用回去伙伴身邊嗎？你的伙伴該不會只有那幾隻吧？」
「嗶嘰嘰嘰⋯⋯」

史萊姆變扁了一些。好像有點垂頭喪氣的感覺？
我又轉身向前，再次踏出步伐。走了一段路之後，又停下腳步回頭。

「嗶嗶嗶⋯⋯」

傷腦筋耶。

「你打算就這樣跟著我嗎？」
「⋯⋯？」

──不行嗎？

可能是在遺跡裡碰上的盡是魔物的關係吧？
我大概可以知道對方的意思──好像是。

「史萊姆是嗎⋯⋯」

看起來不像凶暴的魔物⋯⋯

該怎麼說才好呢？它並沒有遺跡裡的魔物那種「陰森」的感覺。
要我說的話──就是瘋狂的金色。它沒有那種感覺。
這世界也有並不危險的魔物嗎？
不⋯⋯仔細想想，人類也一樣。
有桐原和小山田那樣的人，也有十河和鹿島那樣的人。

「話是這麼說，可是帶著魔物，不知道人家會不會讓我進鄉鎮或村莊⋯⋯」
「噗嗚嗚嗚⋯⋯」

我可以感受到它非常沮喪。

為什麼這時候，過去的記憶會突然一閃而過呢？
以前，我帶了一隻非常虛弱的貓去看獸醫。
沒錯──我記得同班的鹿島小鳩好像也跟我在一起。
腦海裏突然浮現當時的記憶，是因為剛才想起鹿島名字的關係嗎？

那之後，鹿島把貓帶回去養了。
我嬸嬸對貓毛過敏。所以我家不能收留它。
我把貓寄放在獸醫那裡，離開醫院時，我看了貓的臉。貓看起來非常惶恐。

──謝謝你救了我。可是以後我該怎麼辦？

不可思議地，我似乎聽見它害怕的聲音。
貓脖子上沒有項圈。應該是一隻流浪貓。
原來那只貓也是孤伶伶的啊。周圍也沒有擔心地看著它的貓。
它一直孤獨地四處徘徊嗎？
我現在還記得，我對那只貓有一種不可思議的親切感。我知道理由。

大概是因為那只貓跟現在的「我們」一樣。

「原來如此，你也──」

是被群體排除在外的，孤獨的存在。

「跟我一樣，是邊緣人嗎？」


□

鹿島小鳩告訴我她收養了貓時，我衷心感謝她。

幾天後，我叫住鹿島想跟她道謝。但是，她卻尷尬地垂下視線離開了。她的反應並不是討厭我──我是這麼認為的。
鹿島是個內向害羞的女生。我從不曾看過她下課時間在教室跟男生說話的模樣。所以她大概是不知道怎麼應對吧？
最後，我只好默默離開。
說不定，她有一天會自己主動跟我說話。

──如果她不討厭我的話。

要是她討厭我，那麼事情就到此結束。
但是，若她不討厭我，我就沒必要心急。

人和人之間，沒必要急著建立關係。
慢慢來就好。
叔叔和嬸嬸的建議，現在也確實在我的心裡生根茁壯。


▽

我坐在樹蔭下，啃著炸豬排口味點心。

我確認皮囊的情況，發現它的功能復活了。因此決定休息一下，順便用餐。
旁邊的史萊姆不停抖動著。
它緊盯著扁平長方形的「那個」

炸豬排口味的點心。偶爾會讓人很想吃的味道。我記得在原本的世界時，會定期買一些來吃。表面的面皮咬起來口感恰到好處。裡面則有獨特的彈力。我記得好像是魚肉做的？

表面淋上深咖啡色的醬汁。味道偏濃。咸咸的滋味中，帶著微微的甜味。
口感濕潤、硬度適中。兩者合一，令人難以抗拒。口腔內全被它酥脆又強烈的滋味佔據。雖然份量並不多，但是莫名地令人感到滿足。

我灌入一口綠茶。嘴裡變得清爽無比。

「呼！」

或許是走到地面上的關係吧？心理上也比較有食欲能好好享受這些食物。
我看著手上的點心。只剩一口的份量。

「⋯⋯你要吃看看嗎？」

凡事都要試看看才知道。我將那口點心遞給一臉稀罕地看著它的史萊姆。
它身體一部分如突起物般伸了出來。

「嗶嘰⋯⋯？」
「嗯？你問我可不可以吃嗎？」

史萊姆身體變成綠色──表示YES。

「啊啊，當然可以。」

我又往前遞出點心。它突起的前端延伸，包住點心。
點心往史萊姆的體內移動，逐漸融化在半透明的體內。
哼嗯。原來是這樣吃東西的啊！

「啾咿咿咿～♪」

這次變成淡粉紅色。是開心的表現。似乎很合它的心意。

我剛才利用史萊姆做了一個小實驗。
檢驗我們是否能溝通。
我說的話，某種程度上，它似乎能夠理解。

而史萊姆也有方法能夠表示它的想法。
綠色是「肯定」。紅色是「否定」。粉紅色是「善意」或「開心」

現在知道的是這三種類。
說不定還有其他的。不過現在這樣就夠了。

這種魔物的感情表現，比我想像的還豐富。雖然我們沒有共同的語言，而且它臉上也面無表情，卻能清楚傳達出它的想法。說不定還比人類容易理解。
另外，史萊姆也是非常靈巧敏捷的魔物。
只不過，魔物能不能進入城鎮或村莊？

這是現在最大的問題。但這個問題立刻露出解決的預兆。
史萊姆的身體可以變得像繩子一樣細長。
只要把它纏在身體上，再穿上長袍，就看不出來了。

「再來⋯⋯只要城鎮或村莊沒有任何刻意察覺魔物氣息的東西，這個問題就解決了！」

我又發現這個把它「安裝」在我身體上的方法，還有另一個好處。
我站起身來，指示史萊姆纏在我身上。
變成繩子狀的史萊姆從我腳上攀爬上來。上半身隔著衣物也能感受到它柔軟的觸感。有東西爬過脖子後面，感覺癢癢的。史萊姆露出臉來。

「嗶嘰！」
「你看得見後面嗎？」
「嗶！」

突起物伸向我的臉。前端變成「肯定」的綠色。

「很好⋯⋯」

如果我背後有什麼東西出現，這傢伙會通知我。雖然只是一時應急的辦法，但是好歹也算獲得背後的「眼睛」。也可以暫時用這個辦法來對應死角。

「選你當伙伴，似乎是正確選擇呢！」
「嗶嘰♪」

史萊姆恢復原狀。我再次坐下。

其實，我還有一件在意的事。
我從皮囊裡取出『禁術大全』，盤腿坐著翻開書頁。
史萊姆伸出突起處，偷看書頁的內容。

「我記得好像是在這一頁附近──」

我回溯先前在遺跡內快速翻頁時的記憶。

「有了！」


～～～～～～～～～～～～～～～～～～～～

『魔物強化體系草案・實驗結果』
『製作魔物強化劑（進化促進劑）』
『史萊姆→可』
『第一實驗，成功』
『第二實驗，成功』
『第三實驗，成功』
『凶暴化等，對魔物的負面影響，現在還無法確認』


～～～～～～～～～～～～～～～～～～～～

下面快速隨手寫了一行筆記。

『我與史萊姆的關係變得比之前更加好了。我不能否定，我對它產生了感情。傷腦筋。我也因此開始喜歡上了史萊姆。』

大賢者似乎變成了實驗對象的俘虜。

「不過居然有魔物強化劑這種東西啊？」

有了這個，說不行就能大幅提升史萊姆的性能。我朝史萊姆一瞥。

「這麼說來，這些傢伙有提升等級的概念嗎⋯⋯？」

不管怎樣，我也正好想驗證看看。只不過，現在有些事情令我有些在意。
大賢者「故意」研究了強化劑。仰賴強化劑增加魔物能力＝魔物無法跟召喚勇者一樣提升等級。
也可以說是因為這樣，才開發了強化劑。

「只不過這個⋯⋯如果拿來用在惡途的話，說不定是相當危險的研究耶⋯⋯」

我看著那一頁發出低聲呢喃。人類故意強化魔物，是嗎？
我似乎理解大賢者將這些內容歸類為「禁術」的心情了。
我仔細閱讀書頁，發現上頭也寫了可做為強化劑原料的材料。
也寫了哪裡可以獲得那些材料。只不過，當然都是沒看過的地名。

「如果在找尋看得懂禁咒的人途中，可以獲得這些材料的話，就順便弄到手吧！」

接下來──

「差不多該走了。」
「嗶！」
「呃──」

我正準備呼叫史萊姆，卻說不出話來。

「嗶？」

看來⋯⋯還是取個名字比較好。
它的叫聲是「嗶嘰──！」，又圓滾滾的。

「就叫嗶嘰丸吧。」
「嗶嘰？」
「從今天起，你的名字就叫嗶嘰丸。我接受你提出異議，不過──」
「嗶機！」

變化成綠色。

「嗶嘰嘰～♪」

接下來變成粉紅色。看來可以解釋成它很開心。
我拿起皮囊，收起『禁術大全』

裡面還裝著轉移過來的食物被吃完後剩下的垃圾。
考慮到氣味的問題，我試著盡量清除掉沾附在垃圾上的污垢，但是⋯⋯

「嗅嗅！」

或許是因為魔法道具的緣故吧？不可思議地，皮囊內一直都沒有味道。從裡頭拿出來的『禁術大全』也沒有沾上任何異味。雖說如此，若有機會，我還是想要其他袋子。也想把堆積的垃圾處理掉。

『不可以亂丟垃圾喔？』

嬸嬸總是這樣教我。可以遵守的事情，我想好好遵守。

「不知道嬸嬸過得好不好⋯⋯」

她人非常好，好到反而讓人擔心她。

「她那麼親切溫柔⋯⋯一定會擔心我吧⋯⋯」


△

那是親生父母失踪之後，我第一次見到叔叔和嬸嬸那天發生的事。

嬸嬸緊緊抱住尚且年幼的我。她的雙手和聲音都在顫抖。
一開始，我以為她在生氣。

酗酒又成天發脾氣的父親，雙手也總是不停顫抖。
開口就是叫喊怒罵的母親，聲音也總是在顫抖著。

「沒能注意到你，真是對不起！」

嬸嬸向我道歉。
剛開始，我無法理解嬸嬸在道歉什麼。

後來，我才明白那句話的意義。當我明白個中涵義時，我哭了。
我是喜極而泣的。人在開心時，也會哭泣。

對當時的我而言，第一次的經驗令我非常訝異。


▽

我扛著皮囊，將史萊姆──嗶嘰丸纏繞在身上，邁出腳步。
目的地是城鎮或村莊。

「喂，嗶嘰丸。」

嗶嘰丸扭動延伸，伸展到我肩膀附近。

「嗶？」
「我打算做的事情，是個人的復仇。根據不同的看法，我的目的看起來可能很無聊，但是對我來說是非常重要的目標⋯⋯」

人生受限於復仇很愚蠢。
報了仇又能怎樣？報了仇又剩下什麼？
為了復仇而活的人生，實在太遜了。復仇是錯的。復仇毫無意義。
或許有人會這麼想。畢竟──

拘泥於復仇的模樣，有時只顯得悲哀。
執著於復仇的身影，有時只落得淒慘。
但是，我還是要報仇！
為了誰？不用說──當然是為了自己！

為了讓我服氣。為了讓我的感情，能夠有個了結。
什麼「為了千萬人的崇高理想」，去吃屎吧！

從邪惡手中保護世界的救世勇者？
這件事對我來說就像另一個世界的東西，毫無關係。我的目的，歸結於完全的自私。

只不過，這場復仇也包含了正當的大義。
由我為了我自己，執行只屬於我的大義。

如果接下來得到了伙伴，我會告訴對方，這是一場以復仇為目的的旅行。
我決定將毫不猶豫、一點也不隱瞞地告訴對方。至於對方要留要走，端看他自己。

「所以，這場旅行的目的是為了報仇。你接下來只是在配合我滿足私心──這樣可以嗎？」
「嗶！」
「如果你要走，就得趁現在喔？即使你現在就離開，我也不會有怨言。」
「嗶嗶！」

突起的部分，變成了否定的紅色。

「⋯⋯你願意陪我一起走上復仇之旅嗎？」
「嗶！」

肯定的綠色。

「哼，這樣嗎？」

我輕輕撫摸延伸到我脖子旁邊的突起處，踏出下一步。

「既然如此，那麼──」

兩個廢柴邊緣人的旅行。

「就請你多多指教了，我的搭檔！」
「嗶嘰！」

身體顏色變得更加鮮艷的嗶嘰丸，發出輕快的叫聲。
我撥開小樹枝，在森林中前進。

『復仇不會產生任何結果。』
「錯了！」

我並不是希望產生什麼結果。
我不會讓復仇產生任何結果。

「等我準備好了──會毫不留情地毀掉一切！」

這是為了她所進行的復仇。
混帳女神。

「我一定會報仇！」