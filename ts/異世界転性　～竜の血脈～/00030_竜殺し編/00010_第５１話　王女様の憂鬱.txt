「嘿」

倒下

「呀」

倒下

「呼」

伊琳娜揮舞著大劍將盜賊們擊潰

「臥槽，什麼鬼，這個女人！？」

可怕，太可怕了，被大劍斬死還說的過去，可盜賊們竟然都是被砸死的。
５０名盜賊現在已經死傷過半，想要撤退，正在這是，火焰的魔法從天而降，如雨一般落下

「還想逃跑呢，真是天真」

薩基的火焰從天而降，將盜賊全部消滅了

───

「之前你對劍的使用方法相當不好啊」

吃驚的希茲娜對著之前用劍戰鬥的伊琳娜說道

「應該用劍刃將敵人斬殺才對吧，難得的奧里哈鋼大劍，你卻拿它當作鈍器來使用。」
「這也是沒辦法的吧，本來竜就不使用這種人類的武器來戰鬥的嘛」
「你這孩子⋯⋯那就讓我來教你正確的使用方法吧」

說著，希茲娜將眼前盜賊的屍體切碎，屍體散落的到處都是。
但是，總有點不協調的感覺。
卡洛斯察覺到。
莉雅斬殺的屍體也有點太少了吧

「小姐，怎麼了？⋯⋯」

順著那個聲音回頭，少有的表情在臉上浮現出來。

「總覺得好像也沒錯啊⋯⋯」

最後還是放棄了思考，雖然覺得有些不安。

「我好像變弱了啊。」

這句話真是很難讓人信服

「也就是說，雖然身體突然成長了很多，但總感覺沒什麼變化啊。」

卡洛斯馬上就理解了她的意思。這是成長期的男孩（？）是常有的事。

「啊，訓練的時候，也覺得很奇怪呢⋯⋯」

就好像帶著手套再握著刀一樣的感覺
砍人總覺得好像是用竹刀砍的感覺一樣
再這樣下去，鍛鍊果然是必須的吧。

「還有，胸部，果然還是⋯⋯⋯⋯太大了吧」

慣性定律，在這個世界上也是存在的。
在偉大的物理法則下，胸部的脂肪果然還是很礙事啊

「薩基，你的魔道書裡，應該有記載讓胸部變小的魔法吧⋯⋯」
「雖然暫時找不到，不過嘛，也不能說是肯定沒有的吧⋯⋯」

找到也不會幫你變小的吧⋯⋯

莉雅的憂郁仍舊持續著

───

到達迷宮城市的宅邸後，克勞斯訪問後莉雅對後續的事件進行了報告。

宮廷中十分重視的大臣被暗殺的事，不同派別之間的上級貴族也互相懷疑起來。
簡單的說，就是宮中發生了混亂。

「這是科爾多巴搞的鬼吧」

莉雅說的這句雖然沒有根據，但是我總覺得壊事總是出自科爾多巴之手

「不，暗殺大臣是需要相當必要的準備的⋯⋯」

熟悉宮廷內部警戒情況的卡洛斯反駁到。

「那～，有可能是魔族吧。」

薩基的這個意見，點頭的人有。但是也有反對意見。

「魔族侵入了卡薩莉雅的宮廷來暗殺嗎？？」

那是由於人暗殺更難，所以莉雅是這樣想的。
熟知安奈伊亞斯魔法警備的露露也點頭了

「不、這暗殺應該是有人誘導的」
「如果不知道宮廷內權力的情況，有效的暗殺是不可能的。」

總之就是目前所了解的情報還不足

「用虛言感知馬上就能看破吧」

薩基知道那個魔法。但是，目前還是不行的。
不了解魔法的本質，是不可能的。

「使用魔法的人也使用了虛言怎麼辦？使用了虛言感知對抗的魔法，怎麼辦？」

過去盧法斯還在的時候，虛言感知是在審判中作為最後手段來使用的。
但是，隨著盧法斯的死去，他的權威也就消失了。
即使是作為公主的莉雅被信賴著，但是１１歳的孩子說的話，誰會相信嗎？。

「雖然魔法是接近萬能的，但由於人類並不是萬能的緣故」
「有想回安奈伊亞斯一次麼？」

提出建議的是卡洛斯。

莉雅也並不是沒有考慮到這樣的選擇。
攻略不死迷宮和暗黑迷宮，與各地的權威人士熟識並且有強力隊友，打敗了王國最強的騎士的公主，可以說是傳說中的英雄事跡了。

這樣的人物如果回到宮廷中，她的存在，也可能得到盧法斯培養的宮廷魔法師和以拉斯為首的騎士團的支持

「但是老實說這樣會捲入宮廷中的權利鬥爭吧，請饒了我吧」
「那樣的話乾脆姐姐你當國王不就好了？」

聽到這句話，卡洛斯與露露呆住了。
前世是日本人，現今是農民孩子的薩基不知道，那樣的想法是不可能的，而在王宮服侍著的２人是知道的。

「啊，那也不錯啊，大姐頭的話，當國王」

紀咕也同意。瑪露也點頭著表示贊同。

「這樣說來，卡薩莉雅初代國王不就是女王麼。另外，這樣莉雅當女王不是也沒問題？」

希茲娜用事不關己的態度說著這件事

「你們⋯⋯」

莉雅聽到他們說的，皺起了眉頭

───

莉雅是私生子。母親是平民。
作為權力背景的話，迷宮征服者的名聲，與國外的實力者們的關係

但是，作為王的父親的正式的妻子所生，莉雅的弟弟們。他們的母親們當然也是貴族，在國內也有強有力的後盾。
莉雅當上女王的話會有人支持嘛？沒有，絶對沒有

父王的話，即使是這樣也會支持我的吧。
如果，如果莉雅當上王的話，那樣不僅是弟弟們，包括王族也必須全部抹殺。

即便把王族都收拾了，但仍有著繼承王族血脈的貴族存在，所以莉雅的王位也不會穩固吧
此外，還有其他國家嫁進來的王族。那些人認為莉雅血脈不純正也會有想讓莉雅退位的想法這樣別說是內亂了，甚至會引起戰爭吧

被捲入絶贊內鬥中的莉雅，一副完全讓人笑不出來的樣子

「就是這樣啊。」
「嗯，但是，這樣下去的話，果然權力鬥爭還會持續下去的吧？」

薩基交叉雙臂。想起前世的歷史，進入了不一樣的思考模式。

「如果科爾多巴攻打過來的話，很難辦的吧？」
「很難辦⋯不過，父親的話應該能想到什麼辦法吧」

卡薩莉雅王內阿蘇，有這樣的才能。善良，公正，然後冷靜而又透徹的王。
即便再怎麼疼愛女兒，在現實上王位繼承權也不會給吧，在政治的角度上也是正確的理解。

「姐姐的父親，難道是個明君？」
「唉，雖然是個明君。但在戰爭方面大概是很弱的吧，不過，雖然那都是交給手下處理的事。但父親的話，更希望卡薩莉雅的安寧吧⋯⋯」

說到這裡，莉雅是注意到了。

父親有這樣的想法，才能治理好卡薩莉雅吧
如果現在，父親也被暗殺的話，怎麼辦？

「父親有可能會因為這樣而被暗殺者盯上嗎？」

卡洛斯與露露再次僵硬了。

「不會的，也有拉斯先生，即便盧法斯老師不在了，之後也魔法防御鐵壁」

卡洛斯說，莉雅感到了不安。

「比如說我父親被暗殺的話，能阻止得了嗎？」
「不能，不過，如果大小姐在陛下身邊的話也許。」

有那樣的警備制度，不可能讓可疑的人接近吧

「那麼，我果然是不回去比較好吧。國家為中心並要求他們，在這個周邊地區匯和相互合作吧」

這樣的話是在克勞斯房子裡的一個房間裡說的
房子的主人一會要有話和莉雅我說，就讓其他無關的人都退下了

───

「你們的話我都聽到了！那麼就去毀滅王國吧！」

彭！門被突如其來的打開了，進來了一個少女。
由於突然的闖入，室內的氣氛一下子沉靜了下來

打開門的人，好像沒有察覺到這裡的氣氛似的
亞麻色頭髮的美麗的少女。但是從全身引發的霸氣，直覺告訴我這不是普通人
真的，該怎麼說呢，到這裡都還沒有注意到嗎？。

「不，不會去毀滅王國的」

拍著少女的頭進來了的銀髮的精靈。
不，應該說是皮膚白色的黑暗精靈

「啊⋯⋯」

薩基一瞬間明白了怎麼回事，但瑪露恐怕無法理解吧。
因為種族的關係，如果皮膚的顏色變了的話，就無法辨別了。

「零⋯桑？」
「哎呀，好久不見了。順便說一下卡薩莉雅是不會滅亡的，萬一真的毀滅了，至少也與我無關就是了」

魔將軍零帶著親切的笑容說到。